from django.test import TestCase, Client
from django.urls import resolve 

from django.contrib.auth.models import User


# Create your tests here.
class Lab9UnitTest(TestCase):
    def test_lab9_tidak_ada_url(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_ada_url(self):
        response = Client().get('/login/')
        self.assertFalse(response.status_code == 404)

    def test_lab9_ada_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response,'login.html')
        # response1 = Client().get('/regist/')
        # self.assertTemplateUsed(response1,'regist.html')
        response2 = Client().get('/loginFinish/')
        self.assertTemplateUsed(response2,'logout.html')
    
    def test_lab9_bisa_login(self):
        user = User.objects.create_user('fajar', 'fajar245.fa@gmail.com', 'qwerty123')
        response = Client().post('/login/', {'user_id':'fajar', 'password':'qwerty123'})
        self.assertEqual(response.status_code,302)

    
    def test_lab9_tidak_bisa_login(self):
        user = User.objects.create_user('fajar', 'fajar245.fa@gmail.com', 'qwerty123')
        response = Client().post('/login/', {'user_id':'fajar', 'password':'qwerty456'})
        self.assertEqual(response.status_code,200)

    def test_lab9_bisa_logout(self):
        response = Client().post('/logout/')
        self.assertEqual(response.status_code,302)
        
    
    
