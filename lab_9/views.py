from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User

from django.urls import reverse


# Create your views here.
def login_user(request):
    response = {}
    if request.method == "POST":
        user_id = request.POST['user_id']
        password = request.POST['password']
        user = authenticate(request, username=user_id, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('homeLogin:loginFinish'))
        else:
            response["user_not_found"]= "User is doesn't exist"
            return render(request, 'login.html', response) 
            # HttpResponse("Username and Password is not created")
    else:         
        return render(request,'login.html',response)

def loginFinish(request):
    response = {}
    response['user'] = request.user
    return render(request, 'logout.html', response)

def logout_user(request):
    if request.method == "POST":
        logout(request)
    return HttpResponseRedirect(reverse('homeLogin:login'))