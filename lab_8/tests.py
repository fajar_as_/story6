from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .views import dataBuku
# Create your tests here.
class Lab8UnitTest(TestCase):
    def test_lab8_ada_url(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_lab8_ada_func_index(self):
        found = resolve('/book/')
        self.assertEqual(found.func, index)
    
    # def test_lab8_ada_func_dataBuku(self):
    #     found = resolve('/book/')
    #     self.assertEqual(found.func, dataBuku)
    
    def test_lab8_ada_template(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'book.html')

    # def test_lab8_ada_input_atau_ga(self):
    #     response = Client().get('/book/')
    #     self.assertContains(response, '</input>')

    def test_lab8_ada_button_search_atau_ga(self):
        response = Client().get('/book/')
        self.assertContains(response, '</button>')
    
    # def test_lab8_ada_table_atau_ga(self):
    #     response = Client().get('/book/')
    #     self.assertContains(response, '</table>')

    # def test_lab8_ada_tablehead_atau_ga(self):
    #     response = Client().get('/book/')
    #     self.assertContains(response, '</thead>')
    
