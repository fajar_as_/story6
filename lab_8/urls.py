from django.urls import path
from . import views

app_name = 'homeBook'

urlpatterns = [
    path('book/', views.index , name ='index'),
    path('dataBuku', views.dataBuku),
]
