from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('lab7/', views.index , name ='index'),
]
