$(document).ready(() => {
    $.ajax({
        method: 'GET',
        url: '/dataBuku?key=naruto',
        success: function (response){
            let booksView = $('#ini');
            booksView.empty();
            const responseList = response.items;
            for (let i = 0; i< responseList.length; i++){
                let book = responseList[i].volumeInfo;
                booksView.append(
                    "<div class='col-lg-3 col-sm-4 mb-4'>" +
                    "<div class='card h-75'>" +
                    "<img class='card-img-top h-75' src=" + book.imageLinks.thumbnail + ">" +
                    "<div class='card-body'>" +
                    "<h4 class='card-title'>" + book.title + "</h4>" +
                    "<p class='card-text'>Year Released: " + book.publisher + "</p>" +
                    "<p class='card-text'>Categories: " + book.categories + "</p>" +
                    "<p class='card-text'>Authors: " + book.authors + "</p>" +
                    "</div>" +
                    "</div>" +
                    "</div>"
                );
            }
        }
    });
    $('#button').click(function() {
        event.preventDefault();
        const key = $('#search').val();
        $.ajax({
            method: 'GET',
            url: '/dataBuku?key='+key,
            success: function (response){
                let booksView = $('#ini');
                booksView.empty();
                const responseList = response.items;
                for(let i = 0; i< responseList.length; i++){
                    let book = responseList[i].volumeInfo;
                    booksView.append(
                        "<div class='col-lg-3 col-sm-4 mb-4'>" +
                        "<div class='card h-75'>" +
                        "<img class='card-img-top h-75' src=" + book.imageLinks.thumbnail + ">" +
                        "<div class='card-body'>" +
                        "<h4 class='card-title'>" + book.title + "</h4>" +
                        "<p class='card-text'>Year Released: " + book.publisher + "</p>" +
                        "<p class='card-text'>Categories: " + book.categories + "</p>" +
                        "<p class='card-text'>Authors: " + book.authors + "</p>" +
                        "</div>" +
                        "</div>" +
                        "</div>"
                    );
                }
            }
        })
    })
})