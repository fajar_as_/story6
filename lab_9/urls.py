from django.urls import path
from . import views

app_name = 'homeLogin'

urlpatterns = [
    path('login/', views.login_user , name ='login'),
    # path('hello/', views.hello , name ='hello'),
    # path('regist/', views.regist_user , name ='regist'),
    path('loginFinish/', views.loginFinish , name ='loginFinish'),
    path('logout/', views.logout_user, name='logout'),
]
