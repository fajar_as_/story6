from django.test import TestCase, Client
from django.urls import resolve
from .views import index

import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Lab7UnitTest(TestCase):
    def test_lab7_ada_url(self):
        response = Client().get('/lab7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_ada_func_index(self):
        found = resolve('/lab7/')
        self.assertEqual(found.func, index)

    def test_lab7_ada_template(self):
        response = Client().get('/lab7/')
        self.assertTemplateUsed(response, 'index.html')

    def test_lab7_ada_hallo_fajar_anugerah(self):
        response = Client().get('/lab7/')
        self.assertContains(response, 'Hallo, Fajar Anugerah')

    def test_lab7_ada_button_ubah_tema(self):
        response = Client().get('/lab7/')
        self.assertContains(response,'Ubah Tema</button>')

    def test_lab7_ada_accordion(self):
        response = Client().get('/lab7/')
        self.assertContains(response,'accordion')
        
    def test_lab7_ada_aktivitas(self):
        response = Client().get('/lab7/')
        self.assertContains(response,'Aktivitas')

    def test_lab7_ada_Organisasi(self):
        response = Client().get('/lab7/')
        self.assertContains(response,'Organisasi')
    
    def test_lab7_ada_Prestasi(self):
        response = Client().get('/lab7/')
        self.assertContains(response,'Prestasi')

class Lab7FunctionalTest(TestCase):

    def setUp(self):
        # chrome_options = Options()
        # self.selenium  = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
        # super(Lab7FunctionalTest, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        
        selenium.get('http://127.0.0.1:8000/lab7')
        accor_1 = selenium.find_element_by_id('satu')
        accor_2 = selenium.find_element_by_id('dua')
        accor_3 = selenium.find_element_by_id('tiga')
        ubah = selenium.find_element_by_id('ubah')
        css1 = selenium.find_element_by_id('css1')
        css2 = selenium.find_element_by_id('css2')

        time.sleep(1)
        accor_1.click()
        var = accor_1.get_attribute("class")
        self.assertNotIn(var, 'ui-state-active')
        time.sleep(1)

        accor_2.click()
        var2 = accor_2.get_attribute("class")
        self.assertIn('ui-state-active',var2)
        time.sleep(1)

        accor_3.click()
        var3 = accor_3.get_attribute("class")
        self.assertIn('ui-state-active',var3)
        time.sleep(1)

        ubah.click()
        time.sleep(2)

        var4 = css1.get_attribute("href")
        self.assertIn('/static/css/style3.css',var4)
        time.sleep(1)
        ubah.click()
        var4 = css2.get_attribute("href")
        self.assertIn('/static/css/hhh.css',var4)
        time.sleep(2)