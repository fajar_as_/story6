from django.test import TestCase, Client
from django.urls import resolve
from .views import index, tambahStatus
from .forms import Status
from .models import Kabar
from django.views.decorators.http import require_POST

import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


from selenium.webdriver.common.action_chains import ActionChains
# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_lab6_ada_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_lab6_ada_func_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_lab6_ada_hello_apa_kabar(self):
        response = Client().get('/')
        self.assertContains(response, 'Hello, Apa Kabar?')
    
    def test_lab6_template_yang_dipakai_ada(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')
    
    def test_lab6_bisa_buat_status_baru_kemodel(self):
        Kabar.objects.create(status='Aku baik-baik saja')
        self.assertEqual(Kabar.objects.all().count(),1)

    def test_lab6_ada_form_atau_ga(self):
        response = Client().get('/')
        self.assertContains(response, '</form>')

    def test_lab6_ada_button_submit(self):
        response = Client().get('/')
        self.assertContains(response,'</button>')
    
    def test_lab6_ada_func_tambahStatus(self):
        found = resolve('/tambahStatus/')
        self.assertEqual(found.func, tambahStatus)
        
    def test_lab6_representasi_string(self):
        statu = Kabar(status = 'Aku baik-baik saja')
        testStr = 'Aku baik-baik saja'
        self.assertEqual(str(statu),testStr)
        
    def test_lab6_teks_kosong_tidak_masuk_kemodel(self):
        status = Status(data={'status': ''})
        self.assertFalse(status.is_valid())
        self.assertEqual(
            status.errors['status'],['This field is required.']
        )
    
    def test_lab6_teks_masuk_kemodel(self):
        response = Client().post('/tambahStatus/', {'status':'Aku baik-baik saja'})
        self.assertEqual(response.status_code, 302)

        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn('Aku baik-baik saja', html_response)

    def test_lab6_teks_gagal_masuk_model_jika_kosong(self):
        response = Client().post('/tambahStatus/', {'status':''})
        self.assertEqual(response.status_code, 302)

        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertNotIn('Aku baik-baik saja', html_response)
        

class Lab6FunctionalTest(TestCase):

    def setUp(self):
        # chrome_options = Options()
        # self.selenium  = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
        # super(Lab6FunctionalTest, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        
        selenium.get('http://127.0.0.1:8000/')
        
        form = selenium.find_element_by_id('id_status')
        time.sleep(3)
        form.send_keys('Coba Coba')
        time.sleep(3)
        submit = selenium.find_element_by_id('submit')
        time.sleep(3)
        submit.click()
        time.sleep(3)